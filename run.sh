#!/bin/sh
# version of master branch
git tag -l --sort=version:refname "v*"|awk '$1 ~ /^v[0-9]+\.[0-9]+$/'|tail -n 1 > git_master.ver
echo '0'>git_commit_num.temp
git shortlog -sn --no-merges $(cat git_master.ver)..${WERCKER_GIT_COMMIT}>>git_commit_num.temp
awk '{c += $1} END {print c}' git_commit_num.temp > git_master.num
# version of develop branch
git tag -l --sort=version:refname "v*b"|tail -n 1 > git_develop.ver
echo '0'>git_commit_num.temp
git shortlog -sn --no-merges $(cat git_develop.ver)..${WERCKER_GIT_COMMIT}>>git_commit_num.temp
awk '{c += $1} END {print c}' git_commit_num.temp > git_develop.num
# version of feature branch
git tag -l --sort=version:refname "v*a*"|tail -n 1 > git_feature.ver
echo '0'>git_commit_num.temp
git shortlog -sn --no-merges $(cat git_feature.ver)..${WERCKER_GIT_COMMIT}>>git_commit_num.temp
awk '{c += $1} END {print c}' git_commit_num.temp > git_feature.num

# print file contents
echo "<master>"
cat git_master.ver
cat git_master.num
echo "<develop>"
cat git_develop.ver
cat git_develop.num
echo "<feature>"
cat git_feature.ver
cat git_feature.num
